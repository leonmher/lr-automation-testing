#this file is going to perform the following actions...
#1. login as moderator
#2. go to the "Bulk Text to Businesses" page
#3. upload wrong formatted number

#CAUTION
#Be careful with this file since it's gonna send out a real SMS
#Check the very bottom of the file to make sure that the confirm button is not commented out

import time
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

short_wait_time = 7
long_wait_time = 30
base_url = "http://dev.loyalreach.com"
def send_keys(param):
    pass

def test_imports():
    print("Hello world")


def test_qapython():

    driver_service = Service(executable_path="C:\Program Files\chromedriver.exe")

    driver = webdriver.Chrome(service=driver_service)

    driver.get(base_url)
    driver.maximize_window()

    login = driver.find_element(By.XPATH, '//*[@id="loginItem"]')

    login.click()

    driver.find_element(By.XPATH, '/html/body/div[2]/div/div[2]/div/div/div/div/div/div[3]/fieldset/div[1]/input').send_keys("moderator@ehi.com")
    driver.find_element(By.XPATH, '/html/body/div[2]/div/div[2]/div/div/div/div/div/div[3]/fieldset/div[2]/input').send_keys("secret", Keys.RETURN)

    #wait until the page loads and click on los tacos locos to open up the dropdown
    try:
        administration = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, "/html/body/div/div/div[1]/div[2]/div[1]/div/div/div/ul/li[1]/a"))
        )
    finally:
        print("moderator")

    administration.click()

    time.sleep(short_wait_time)
    #Bulk text to businesses
    driver.find_element(By.XPATH, '/html/body/div/div/div[1]/div[2]/div[1]/div/div/div/ul/li[1]/ul/li[1]/a').click()

    time.sleep(short_wait_time)
    #fill text field
    driver.find_element(By.XPATH, '/html/body/div/div/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[2]/div/div/textarea').send_keys("from selenium with love")

    #add wrong file type
    driver.find_element(By.XPATH, '/html/body/div/div/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div/div/div/div[2]/div[1]/div[1]/div/div/div/div/div/div/div/input').send_keys('C:\\Program Files\\wrongFormatNumber.xlsx')

    #click add button
    # driver.find_element(By.XPATH, '/html/body/div[2]/div/div[2]/div/div/div[3]/button').click()

    #click on send button
    time.sleep(long_wait_time)
    #confirm "yes" to send out the SMS
    # driver.find_element(By.XPATH, '/html/body/div[2]/div/div[2]/div/div/div[3]/button[1]').click()








######################################################################################
#login page
######################################################################################

#email input
#//*[@id="email"]

#password input
#//*[@id="password"]

#sign in button
#/html/body/div[2]/div/div[2]/div/div/div/div/div/div[3]/fieldset/button


######################################################################################
#Main page
######################################################################################

#Aadministration
#/html/body/div/div/div[1]/div[2]/div[1]/div/div/div/ul/li[1]/a

#Bulk text to businesses
#/html/body/div/div/div[1]/div[2]/div[1]/div/div/div/ul/li[1]/ul/li[1]/a

#new campaign
#/html/body/div/div/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div/div/div[1]/a

######################################################################################
#Create Bulk Text
######################################################################################
#text field
#/html/body/div/div/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[2]/div/div/textarea

#number field
#/html/body/div/div/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div/div/div/div[2]/div[1]/div[1]/div/div/div[1]/input

#name field
#/html/body/div/div/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div/div/div/div[2]/div[1]/div[1]/div/div/div[2]/input

#add button
#/html/body/div/div/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div/div/div/div[2]/div[1]/div[2]/button

#send button
#/html/body/div/div/div[1]/div[2]/div[2]/div[2]/div[2]/div[3]/div/div/div/div/button[1]

#yes confirm button
#/html/body/div[2]/div/div[2]/div/div/div[3]/button[1]