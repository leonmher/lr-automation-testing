Loyalreach Automation Testing

This project uses Python and Selenium under the hood and is meant for testing purposes only. LR automation is gonna make the testing process way easier by reducing the repetative tasks.

Initial setup
In order to get started with LR automation we have to get some things done beforehand...


Have googledriver installed on your machine. Make sure to get the correct version.
https://chromedriver.chromium.org/downloads


Have python installed
https://www.python.org/


Some basic knowledge about python virtual env (VENV) is required
How to create a virtual enviroment?
python -m venv "nameOfProject"
To check which packages we have installed in the virtual enviroment, we run
pip list
To activate the virtual envoroment

go to the scripts folder
in the command line type -  activate.bat

To run the test

cd to the "test" folder
choose the project you want to start e.g. "test_sendSms.py"
run the command - pytest test_sendSms.py
